<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use App\Entity\Post;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DomCrawler\Crawler;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $user1 = new User();
        $user1->add("Jan", "Dzban", "daa1111@aaa.com", new \DateTime('2014-01-01 11:32'));
        $user2 = new User();
        $user2->add("Maciek", "Zzadupia", "asdasd12313@sda.com", new \DateTime());
        $manager->persist($user1);
        $manager->persist($user2);

        $post = new Post();
        $post->add(
            $user1,
            "Przyczepa odłączyła się od tira, zginęło trzech górników. Kierowca i diagnosta oskarżeni",
            new \DateTime('2017-02-11 12:02'),
            "Według śledczych właściciel przyczepy Bartosz W. dobrze wiedział, że jej stan techniczny jest fatalny. Próbował ż",
            new \DateTime('2017-02-11 12:02'));
        $manager->persist($post);

        $comment = new Comment();
        $comment->add($post, $user2, "oj", new \DateTime('2017-02-11 12:05'));
        $manager->persist($comment);

        $manager->flush();
    }
}
